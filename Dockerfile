FROM openjdk:8-jdk
RUN apt-get update && apt-get -y install maven && apt-get clean
COPY server /server/
WORKDIR /server
RUN mvn package
CMD ["mvn", "jetty:run"]
EXPOSE 8080
